-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
					 width = 1500,
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
						 width = 1500,
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init(string.format("%s/.config/awesome/themes/%s/theme.lua", os.getenv("HOME"), "wos"))

-- This is used later as the default terminal and editor to run.
terminal = "alacritty"
editor = os.getenv("EDITOR") or "nano"
editor_cmd = terminal .. " -e " .. editor
web_browser = "librewolf"
newsboatdb = "/home/ant/.newsboat/cache.db"

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.corner.nw,
    awful.layout.suit.floating,
    -- awful.layout.suit.tile.AMONGIleft,
    awful.layout.suit.tile.bottom,
    -- awful.layout.suit.tile.top,
    -- awful.layout.suit.fair,
    -- awful.layout.suit.fair.horizontal,
    -- awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    -- awful.layout.suit.max,
    -- awful.layout.suit.max.fullscreen,
    -- awful.layout.suit.magnifier,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
   { "manual", terminal .. " -e man awesome" },
   { "restart WM", awesome.restart },
   { "quit", function() awesome.quit() end },
}

mymainmenu = awful.menu({
			items = {
					{ "awesome", myawesomemenu},
					{ "open terminal", terminal},
					{ "edit configs", "setsid -f configure.sh"}
                                  },
                        })


mylauncher = awful.widget.launcher({ image = beautiful.wOS_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
local mytextclock = wibox.widget.textclock()
local calandarpopup = awful.widget.calendar_popup.month()
calandarpopup:attach(mytextclock, "tr")

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              awful.menu.client_list({ theme = { width = 250 } })
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ "Web", "Term", "Work", "Misc", "Chat", "VM"}, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
	layout = {
		layout = wibox.layout.fixed.horizontal,
		spacing = 5,
	},
	widget_template = {
		widget = wibox.container.margin,
		left = 2,
		right = 2,
		top = 1,
		{
			widget = wibox.container.background,
			id = "background_role",
			{
				widget = wibox.container.margin,
				top = 3,
				bottom = 3,
				{
					widget = wibox.container.background,
					bg = beautiful.black,
					{
						widget = wibox.widget.textbox,
						valign = "center",
						halign = "center",
						id = "text_role"
					}
				}
			}
		}
	},
        buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons,
		layout = {
				spacing = 20,
				layout = wibox.layout.flex.horizontal,
		},
		widget_template = {
			widget = wibox.container.margin,
			left = 2,
			right = 2,
			top = 3,
			{
				widget = wibox.container.background,
				id = "background_role",
				{
					widget = wibox.container.margin,
					top = 3,
					bottom = 3,
					{
						widget = wibox.container.background,
						bg = beautiful.black,
						{
							widget = wibox.container.margin,
							left = 5,
							right = 5,
							top = 2,
							bottom = 2,
							{
								layout = wibox.layout.fixed.horizontal,
								spacing = 4,
								{
									widget = wibox.widget.imagebox,
									id = "icon_role"
								},
								{
									widget = wibox.widget.textbox,
									font = beautiful.small_font,
									valign = "center",
									halign = "left",
									id = "text_role"
								}
							}
						}
					}
				}
			},
		buttons = taglist_buttons
	},
    }


	-- {{{ music widget 
	s.musicwidget = wibox.widget {
		{
			widget = wibox.container.margin,
			top = 5,
			{
				layout = wibox.layout.fixed.horizontal,
				spacing = 5,
				{
					widget = wibox.widget.imagebox,	
					image = beautiful.music_icon,
					resize = true,
					forced_width = 12,
					forced_height = 12,
					valign = "center",
					halign = "left",
				},
				{
					widget = wibox.widget.textbox,
					font = beautiful.small_font,
					valign = "center",
					text = "",
					id = "label",
				},
			}
		},
		widget = wibox.container.constraint,
		strategy = "exact",
		width = 200,
	}


	s.mini_music_player = awful.popup {
		preferred_position = "bottom",
		hide_on_right_click = true,
		border_color = beautiful.border_focus,
		border_width= beautiful.border_width,
		ontop = true,
		visible = false,
		type = "dock",
		widget = wibox.widget{
			widget = wibox.container.margin,
			top = 10,
			bottom = 10,
			right = 25,
			left = 25,
			forced_width = 300,
			forced_height = 125,
			{
				layout = wibox.layout.fixed.vertical,
				spacing = 5,
				{ -- current song label
					widget = wibox.container.place,
					halign = "center",
					{
						widget = wibox.container.background,
						bg = beautiful.slightly_dark_green,
						{
							widget = wibox.container.margin,
							top = 2,
							bottom = 2,
							{
								widget = wibox.container.background,
								bg = beautiful.black,
								{
									widget = wibox.container.margin,
									left = 10,
									right = 10,
									bottom = 4,
									{
										widget = wibox.widget.textbox,
										text = "not set yet",
										ellipsize = "end",
										forced_height = 15,
										halign = "center",
										valign = "center",
										id = "label"
									}
								}
							}
						}
					}
				},
				{  -- song duration progress bar
					widget = wibox.widget.progressbar,
					max_value = 100,
					min_value = 0,
					value = 0,
					forced_height = 20,
					border_width = 1,
					paddings = 2,
					border_color = beautiful.border_normal,
					color = beautiful.fg_normal,
					background_color = beautiful.bg_normal,
					--shape = gears.shape.rounded_bar,
					--bar_shape = gears.shape.rounded_bar,
					id = "progressbar",
				},
				{ -- the widget containing the control buttons
					widget = wibox.container.place,
					halign = "center",
					forced_width = 280,
					forced_height = 50,
					{
						layout = wibox.layout.ratio.horizontal,
						inner_fill_strategy = "inner_spacing",
						forced_width = 200,
						{ -- the previous button
							widget = wibox.widget.imagebox,	
							image = beautiful.previous_icon,
							resize = true,
							forced_width = 50,
							forced_height = 50,
							valign = "center",
							halign = "center",
							buttons = gears.table.join(
								awful.button({ }, 1, function()
									awful.spawn("mpc prev")
									s.mini_music_player.widget:get_children_by_id("pause-button")[1].image = beautiful.stop_icon
								end)
							),	
						},
						{ -- the play/pause button
							widget = wibox.widget.imagebox,	
							image = beautiful.play_icon,
							resize = true,
							forced_width = 50,
							forced_height = 50,
							valign = "center",
							halign = "right",
							id = "pause-button",
							buttons = gears.table.join(
								awful.button({ }, 1, function()
									awful.spawn.easy_async_with_shell([[mpc status %state%]] , function(out)
										if out == "playing\n" then
											s.mini_music_player.widget:get_children_by_id("pause-button")[1].image = beautiful.play_icon
										else
											s.mini_music_player.widget:get_children_by_id("pause-button")[1].image = beautiful.stop_icon
										end
										awful.spawn("mpc toggle")	
									end)
								end)
							),	
						},
						{ -- the next button
							widget = wibox.widget.imagebox,	
							image = beautiful.next_icon,
							resize = true,
							forced_width = 50,
							forced_height = 50,
							valign = "center",
							halign = "left",
							buttons = gears.table.join(
								awful.button({ }, 1, function()
									awful.spawn("mpc next")
									s.mini_music_player.widget:get_children_by_id("pause-button")[1].image = beautiful.stop_icon
								end)
							)
						}
					}
				}
			}
		}
	}

	local function set_music_label(NewValue)
		s.musicwidget:get_children_by_id("label")[1].text = NewValue
		s.mini_music_player.widget:get_children_by_id("label")[1].text = NewValue
	end

	local function set_duration_procent(NewValue) 
		s.mini_music_player.widget:get_children_by_id("progressbar")[1].value = NewValue
	end

	local function update_player_state()
		awful.spawn.easy_async_with_shell([[mpc status %state%]] , function(out)
			if out == "playing\n" then
				s.mini_music_player.widget:get_children_by_id("pause-button")[1].image = beautiful.stop_icon
			else
				s.mini_music_player.widget:get_children_by_id("pause-button")[1].image = beautiful.play_icon
			end
		end)

		awful.spawn.easy_async_with_shell([[mpc current | xargs -I$ basename $]], function(out) 
			if #out ~= 0 then -- if not name, then it means no song is currently playing, in that case hide the widget
				set_music_label(out)
				s.musicwidget.visible = true
			else
				s.musicwidget.visible = false
			end
		end)
	end

	awful.spawn.with_line_callback("mpc idleloop", {
		stdout = function(out) 
			if out == "player" then
				update_player_state()	
			end
		end
	})



	musicdurationtimer = gears.timer {
		timeout = 5,
		call_now = true,
		autostart = true,
		callback = function()
			awful.spawn.easy_async_with_shell([[mpc status %percenttime% | sed "s/%//"]], function(out)
				set_duration_procent(tonumber(out))
			end)
		end
	}

	update_player_state()

	s.mini_music_player:bind_to_widget(s.musicwidget, 1)
	-- }}}

	-- {{{ timew warrior widget
	s.timewarriorwidget = wibox.widget {
		widget = wibox.container.constraint,
		strategy = "exact",
		width = 125,
		height = nil,
		{
			widget = wibox.container.margin,
			top = 5,
			{
				layout = wibox.layout.fixed.horizontal,
				spacing = 5,
				{
					widget = wibox.widget.imagebox,	
					image = beautiful.cog_icon,
					--resize = true,
					forced_width = 15,
					forced_height = 15,
					valign = "center",
					halign = "left",
				},
				{
					widget = wibox.widget.textbox,
					font = beautiful.small_font,
					text = "",
					id = "label",
				},
			}
		},
	}

	timew_timer = gears.timer {
		timeout = 5,
		call_now = true,
		autostart = true,
		callback = function()
			awful.spawn.easy_async_with_shell([[timew get dom.active]], function(out)
				if tonumber(out)  == 1 then
					s.timewarriorwidget.visible = true
					awful.spawn.easy_async_with_shell([[timew get dom.active.tag.1]], function(out)
						s.timewarriorwidget:get_children_by_id("label")[1].text = tostring(out)
					end)
				else
					s.timewarriorwidget.visible = false
				end
			end)
		end
	}
	-- }}}



	--s.rss_label = wibox.widget {
	--	layout = wibox.layout.fixed.horizontal,
	--	fixed_width = 75,
	--	{
	--		widget = wibox.container.margin,
	--		top = 7,
	--		right = 5,
	--		left = 5,
	--		bottom = 5,
	--		{
	--			widget = wibox.widget.imagebox,	
	--			image = beautiful.rss_icon,
	--			resize = true,
	--			forced_width = 15,
	--			forced_height = 15,
	--			valign = "center",
	--			halign = "left",
	--		}
	--	},
	--	{
	--		widget = awful.widget.watch([[sqlite3 -line ]] .. newsboatdb .. [[ "select count(id) as '' from rss_item where unread = 1"]], 60)
	--	},
	--}

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s, height = 30})

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
		bg = gears.color.transparent,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
	    spacing = 5,
            mylauncher,
            s.mytaglist,
            s.mypromptbox,
        },
		{  -- Middle widget
			widget = wibox.container.margin,
			left = 20,
			right = 20,
			s.mytasklist, 
		},
        { -- Right widgets
		widget = wibox.container.background,
		bg = beautiful.slightly_dark_green,
		{
			widget = wibox.container.margin,
			top = 4, 
			bottom = 4,
			{
				widget = wibox.container.background,
				bg = beautiful.black,
				{
					layout = wibox.layout.fixed.horizontal,
					s.timewarriorwidget,
					s.musicwidget,
					wibox.widget.systray(),
					mytextclock,
					s.mylayoutbox,
				}
			}
		}
        },
    }
end)
-- }}}

-- {{{ Mouse bindings
--root.buttons(gears.table.join(
    --awful.button({ }, 3, function () mymainmenu:toggle() end),
    --awful.button({ }, 4, awful.tag.viewnext),
    --awful.button({ }, 5, awful.tag.viewprev)
--))
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(
    awful.key({ modkey,           }, "b",      function() awful.spawn(web_browser) end,
              {description="open browser", group="launcher"}),
    awful.key({ modkey}, "a",      function() awful.spawn("amixer sset Master 5%-") end,
              {description="increase volume", group="misc"}),
    awful.key({ modkey, "Shift"}, "a",      function() awful.spawn("amixer sset Master 5%+") end,
              {description="decrease volume", group="misc"}),
    awful.key({ modkey, "Shift"           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "s",      function() awful.spawn("/home/ant/scripts/chosescript.sh") end,
              {description="show help", group="awesome"}),
    awful.key({ modkey }, "p", function() awful.spawn("/home/ant/scripts/screenshot.sh") end,
              {description = "make a screenshot", group = "misc"}),
    awful.key({ modkey, "Shift" }, ".", function() awful.spawn("mpc next") end,
              {description = "play next song in mpd", group = "misc"}),
    awful.key({ modkey, "Shift" }, ",", function() awful.spawn("mpc prev") end,
              {description = "play previous song in mpd", group = "misc"}),
    awful.key({ modkey, "Shift"}, "p", function() awful.spawn.easy_async_with_shell("mpc toggle") end,
              {description = "pause mpd", group = "misc"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}
    ),
--    awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
--              {description = "show main menu", group = "awesome"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal(
                        "request::activate", "key.unminimize", {raise = true}
                    )
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Prompt
    awful.key({ modkey },            "r",     function () awful.spawn.with_shell("rofi -show run") end,
              {description = "run prompt", group = "launcher"}),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"})
    -- Menubar
)

clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
              {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"}),
    awful.key({ modkey, "Control" }, "m",
        function (c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end ,
        {description = "(un)maximize vertically", group = "client"}),
    awful.key({ modkey, "Shift"   }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end ,
        {description = "(un)maximize horizontally", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          "Arandr",
          "Blueman-manager",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
          "Wpa_gui",
          "veromix",
          "xtightvncviewer"},

        -- Note that the name property shown in xprop might be set slightly after creation of the client
        -- and the name shown there might not match defined rules here.
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = {}
    },

    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { screen = 1, tag = "2" } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    if not awesome.startup then 
				awful.client.setslave(c)
		end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({ }, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
		--c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
--
-- autostarting programs
awful.spawn.with_shell("picom")
