---------------------------
-- Default awesome theme --
---------------------------

local theme_assets = require("beautiful.theme_assets")
local gears = require("gears")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
--local themes_path = gfs.get_themes_dir()
local themes_path = "/home/ant/.config/awesome/themes/wos/"

local theme = {}

light_orange        =  "#F97620"
dark_orange         =  "#260A02"
dark_orange_redish  =  "#660A02"
dark_orange_little_redish  =  "#460A02"
blue                =  "#207bf9"
dark_blue = "#106be9" 

theme.light_green = "#7FFF7F"
theme.slightly_dark_green = "#212d2d"
theme.dark_green = "#2A542A"
theme.black = "#0b0d0d"

theme.font                    =  "PublicPixel 10"
theme.small_font                    =  "PublicPixel 6"
theme.bg_normal               =  theme.black
theme.bg_focus                =  theme.light_green
theme.bg_urgent               =  theme.gb_normal
theme.bg_minimize             =  theme.black
theme.bg_systray              =  theme.bg_normal  
theme.fg_normal               =  theme.bg_focus   
theme.fg_focus                =  theme.bg_normal  
theme.fg_urgent               =  theme.light_green
theme.fg_minimize             =  theme.light_green
theme.useless_gap             =  dpi(5)           
theme.border_width            =  dpi(2)           
theme.border_normal           =  theme.slightly_dark_green   
theme.border_focus            =  theme.light_green
theme.border_marked           =  "#91231c"        
theme.maximized_hide_border   =  false             
theme.fullscreen_hide_border  =  true             

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

--tasklist
theme.tasklist_bg_focus = theme.dark_green
theme.tasklist_bg_normal = theme.black
theme.tasklist_fg_normal = theme.light_green
theme.tasklist_fg_focus = theme.light_green
theme.tasklist_fg_minimize = theme.dark_green

theme.taglist_fg_focus = theme.light_green
theme.taglist_bg_focus = theme.dark_green

-- Generate taglist squares:
local taglist_square_size = dpi(0)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal 
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]
theme.notification_font = "sans 8"
theme.notification_margin = dpi(100)
theme.notification_width = dpi(400)
theme.notification_height = dpi(75)

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path.."default/submenu.png"
theme.menu_border_color = theme.light_green
theme.menu_height = dpi(15)
theme.menu_width  = dpi(250)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.music_icon     =  themes_path.."music-icons/music-icon.png"
theme.next_icon      =  themes_path.."music-icons/next-icon.png"
theme.previous_icon  =  themes_path.."music-icons/previous-icon.png"
theme.stop_icon      =  themes_path.."music-icons/stop-icon.png"
theme.play_icon      =  themes_path.."music-icons/play-icon.png"
theme.cog_icon       =  themes_path.."music-icons/cog.png"
theme.rss_icon       =  themes_path.."rss-icon.png"

theme.titlebar_close_button_normal = themes_path.."default/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = themes_path.."default/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = themes_path.."default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = themes_path.."default/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = themes_path.."default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = themes_path.."default/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = themes_path.."default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = themes_path.."default/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = themes_path.."default/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = themes_path.."default/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = themes_path.."default/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = themes_path.."default/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = themes_path.."default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = themes_path.."default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = themes_path.."default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = themes_path.."default/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = themes_path.."default/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path.."default/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = themes_path.."default/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = themes_path.."default/titlebar/maximized_focus_active.png"

--theme.wallpaper = themes_path.."default/background.png"
theme.wallpaper = "/home/ant/.config/awesome/themes/wos/background.png"

-- You can use your own layout icons like this:
theme.layout_fairh = themes_path.."layouts/fairhw.png"
theme.layout_fairv = themes_path.."layouts/fairvw.png"
theme.layout_floating  = themes_path.."layouts/floatingw.png"
theme.layout_magnifier = themes_path.."layouts/magnifierw.png"
theme.layout_max = themes_path.."layouts/maxw.png"
theme.layout_fullscreen = themes_path.."layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."layouts/tileleftw.png"
theme.layout_tile = themes_path.."layouts/tilew.png"
theme.layout_tiletop = themes_path.."layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."layouts/spiralw.png"
theme.layout_dwindle = themes_path.."layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."layouts/cornernww.png"
theme.layout_cornerne = themes_path.."layouts/cornernew.png"
theme.layout_cornersw = themes_path.."layouts/cornersww.png"
theme.layout_cornerse = themes_path.."layouts/cornersew.png"

-- Generate Awesome icon:
--theme.awesome_icon = theme_assets.awesome_icon(
--    theme.menu_height, theme.bg_focus, theme.fg_focus
--)

theme.wOS_icon = themes_path.."wOSicon.png"

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
